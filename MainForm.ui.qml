import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4 as QC1

ColumnLayout {
    spacing: 0
    RowLayout {
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
        spacing: 0
        Rectangle {
            id: sideButtons
            width: 200
            color: "#333333"
            Layout.preferredWidth: 50
            Layout.fillHeight: true
            ColumnLayout {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
                Repeater {
                    model: ["😂", "😰", "🛸", "🔬"]
                    Text {
                        color: "#ffffff"
                        text: modelData
                        fontSizeMode: Text.HorizontalFit
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 34
                        leftPadding: 4
                        Layout.minimumWidth: 50
                        Layout.preferredHeight: 50
                    }
                }
            }
        }

        QC1.SplitView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            handleDelegate: Rectangle {
                color: "#1e1e1e"
                width: 1
            }

            Rectangle {
                id: sideBar
                width: 200
                Layout.fillHeight: true
                color: "#252526"
                ColumnLayout {
                    spacing: 0
                    width: sideBar.width
                    Text {
                        text: "EXPLORE"
                        color: "#ccc"
                        font.pointSize: 8
                        Layout.topMargin: 16
                        Layout.leftMargin: 16
                        Layout.bottomMargin: 16
                        renderType: Text.NativeRendering
                    }
                    Rectangle {
                        id: rectangle
                        height: 28
                        color: "#2e2e2f"
                        Layout.fillWidth: true
                        Layout.bottomMargin: 8
                        Text {
                            color: "#ffffff"
                            text: "MY PROJECT"
                            font.pixelSize: 11
                            font.bold: true
                            anchors.verticalCenter: parent.verticalCenter
                            leftPadding: 16
                        }
                    }
                    FileLine {
                        name: "src"
                        open: true
                        indent: 1
                    }
                    FileLine {
                        name: ".git"
                        open: false
                        indent: 2
                    }
                    FileLine {
                        name: "js"
                        open: true
                        indent: 2
                    }
                    FileLine {
                        name: "File1.js"
                        isFile: true
                        indent: 2
                    }
                    FileLine {
                        name: "File2.js"
                        isFile: true
                        indent: 2
                    }
                    FileLine {
                        name: "js"
                        open: true
                        indent: 2
                    }
                    FileLine {
                        name: "py"
                        open: false
                        indent: 2
                    }
                }
            }
            Rectangle {
                color: "#1e1e1e"
                ColumnLayout {
                    id: editorLayout
                    anchors.fill: parent
                    spacing: 0

                    TabBar {
                        id: tabBar
                        width: 240
                        Layout.preferredHeight: 36
                        background: Rectangle {
                            color: "#252526"
                        }
                        FileTab {
                            text: "File1.js   ●"
                        }
                        FileTab {
                            text: "File2.ts"
                        }
                        FileTab {
                            text: "file_3.py"
                        }
                    }
                    StackLayout {
                        currentIndex: tabBar.currentIndex
                        TextArea {
                            id: textArea
                            font.family: "Iosevka"
                            font.pointSize: 13
                            color: "#fff"
                            text: "\nconsole.log(\"Hello from QML, stouset\");\n\nconsole.log(\"Love, mixedCase\");"
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            renderType: Text.NativeRendering
                        }
                    }
                }
            }
        }
    }
    Rectangle {
        color: "#007acc"
        Layout.fillWidth: true
        Layout.preferredHeight: 22
    }
}
