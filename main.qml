import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: "VS Code QML"
    menuBar: MenuBar {
        Menu {
            title: "File"
            Menu {
                title: "Menu"
                MenuItem {
                    text: "Open dialog"
                    onTriggered: dialog.open()
                }
            }
        }
        Menu { title: "Edit" }
        Menu { title: "View" }
        Menu { title: "Goto" }
        Menu { title: "Help" }
    }

    Dialog {
        id: dialog
        title: "Now look at your RAM consumption"
        Text {
            text: "Look at the title"
        }
    }

    MainForm {
        anchors.fill: parent
    }
}
