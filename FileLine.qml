import QtQuick 2.6

Rectangle {
    property string name
    property bool open
    property int indent
    property bool isFile
    height: 24
    Text {
        text: (isFile ? "" : (open ? "▼  ":"▹  ")) + name
        color: isFile ? "#ccc" : "#fff"
        font.family: "Liberation Sans"
        font.pixelSize: 13
        leftPadding: 16 * (indent + (isFile ? 1 : 0))
        renderType: Text.NativeRendering
    }
}
