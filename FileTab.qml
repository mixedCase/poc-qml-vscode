import QtQuick 2.7
import QtQuick.Controls 2.2

TabButton {
    contentItem: Text {
        text: parent.text
        font.family: "Liberation Sans"
        color: parent.checked ? "#fff" : "#959595"
        horizontalAlignment: Text.AlignHCenter
		verticalAlignment: Text.AlignVCenter
        renderType: Text.NativeRendering
        leftPadding: 12
        rightPadding: 10
    }

    background: Rectangle {
        color: parent.checked ? "#1e1e1e" : "#252526"
        implicitHeight: 36
    }
}
